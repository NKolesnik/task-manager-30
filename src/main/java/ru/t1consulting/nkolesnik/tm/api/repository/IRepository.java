package ru.t1consulting.nkolesnik.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    boolean existsById(@NotNull String id);

    @NotNull
    List<M> findAll();

    @Nullable
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    List<M> findAll(@NotNull Sort sort);

    @Nullable
    M findByIndex(@NotNull Integer index);

    @Nullable
    M findById(@NotNull String id);

    @Nullable
    M remove(@NotNull M model);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

    void removeAll(@NotNull Collection<M> collections);

    void clear();

    long getSize();

}
